document.getElementById('task-form').addEventListener('submit', function(event) {
  event.preventDefault();

  // Get the task input value
  var task = document.getElementById('task-input').value;

  // Create a new list item
  var li = document.createElement('li');
  li.className = 'border-2 border-gray-300 rounded px-3 py-2 flex justify-between items-center';

  // Create the task text node
  var taskText = document.createTextNode(task);
  li.appendChild(taskText);

  // Create the delete button
  var deleteButton = document.createElement('button');
  deleteButton.innerHTML = '<i class="fas fa-trash-alt"></i>';
  deleteButton.className = 'text-red-500';
  deleteButton.addEventListener('click', function() {
    this.parentElement.remove();
  });
  li.appendChild(deleteButton);

  // Add the list item to the task list
  document.getElementById('task-list').appendChild(li);

  // Clear the task input value
  document.getElementById('task-input').value = '';
});
